public class App {
    public static void main(String[] args) throws Exception {
        //Construir un objeto de tipo Persona
        Persona objPersona_1 = new Persona("Andres", "Quintero", 25);
        //Obtener el nombre y apellido
        String nombre = objPersona_1.getNombre();
        String apellido = objPersona_1.getApellido();

        System.out.println("Nombre: "+nombre+"\nApellido: "+apellido);

        Persona objPersona_2 = new Persona("Dionisio", "Caro", 20);
        Persona objPersona_3 = new Persona("Juliana", "Mendez", 22);

        objPersona_3.setEdad(25);
        apellido = objPersona_3.getApellido();
        int edad = objPersona_3.getEdad();
        System.out.println(apellido+" tiene una edad de: "+edad+" años");
    }
}
