//Clase que representa a una persona
public class Persona {
    //Atributos
    private String nombre;
    private String apellido;
    private int edad;

    //Constructor
    public Persona(String nombre, String apellido, int edad){
        //this.nombre -> Atributo
        //nombre -> Parámetro
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        //System.out.println("Hola soy un objeto Persona, soy "+nombre);
    }

    /**********************
     * MÉTODOS CONSULTORES
     **********************/
    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }

    public int getEdad(){
        return edad;
    }

    /************************
     * MÉTODOS MODIFICADORES
     ************************/
    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setApellido(String apellido){
        this.apellido = apellido;
    }

    public void setEdad(int edad){
        this.edad = edad;
    }

    /************************
     * ACCIONES
     ************************/
    public double calcularNomina(double valorHora){
        int horas = 10;
        return (horas*valorHora);
    }
}
